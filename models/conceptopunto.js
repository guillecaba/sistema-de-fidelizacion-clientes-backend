'use strict';
module.exports = (sequelize, DataTypes) => {
  const ConceptoPunto = sequelize.define('ConceptoPunto', {
    descripcion: DataTypes.STRING,
    puntosRequeridos: DataTypes.INTEGER
  }, {});
  ConceptoPunto.associate = function(models) {
    // associations can be defined here
  };
  return ConceptoPunto;
};