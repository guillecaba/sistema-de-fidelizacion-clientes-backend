'use strict';
module.exports = (sequelize, DataTypes) => {
  const VencimientoPunto = sequelize.define('VencimientoPunto', {
    fechaInicio: DataTypes.DATEONLY,
    fechaFin: DataTypes.DATEONLY,
    duracion: DataTypes.INTEGER
  }, {});
  VencimientoPunto.associate = function(models) {
    // associations can be defined here
  };
  return VencimientoPunto;
};