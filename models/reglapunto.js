'use strict';
module.exports = (sequelize, DataTypes) => {
  const ReglaPunto = sequelize.define('ReglaPunto', {
    limiteInferior: DataTypes.INTEGER,
    limiteSuperior: DataTypes.INTEGER,
    montoEquivalencia: DataTypes.INTEGER
  }, {});
  ReglaPunto.associate = function(models) {
    // associations can be defined here
  };
  return ReglaPunto;
};