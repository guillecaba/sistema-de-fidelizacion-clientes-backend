'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsoPuntosDetalle = sequelize.define('UsoPuntosDetalle', {
    cabeceraId: DataTypes.INTEGER,
    puntajeUtilizado: DataTypes.INTEGER,
    bolsaId: DataTypes.INTEGER
  }, {});
  UsoPuntosDetalle.associate = function(models) {
    // associations can be defined here
  };
  return UsoPuntosDetalle;
};