'use strict';
module.exports = (sequelize, DataTypes) => {
  const Cliente = sequelize.define('Cliente', {
    nombre: DataTypes.STRING,
    apellido: DataTypes.STRING,
    nroDoc: DataTypes.INTEGER,
    tipoDoc: DataTypes.STRING,
    nacionalidad: DataTypes.STRING,
    email: DataTypes.STRING,
    telefono: DataTypes.INTEGER,
    fechaNacimiento: DataTypes.DATEONLY
  }, {});
  Cliente.associate = function(models) {
    // associations can be defined here
  };
  return Cliente;
};
