'use strict';
module.exports = (sequelize, DataTypes) => {
  const BolsaPuntos = sequelize.define('BolsaPuntos', {
    clienteId: DataTypes.INTEGER,
    fechaAsignacion: DataTypes.DATEONLY,
    fechaCaducidad: DataTypes.DATEONLY,
    puntajeAsignado: DataTypes.INTEGER,
    puntajeUtilizado: DataTypes.INTEGER,
    saldo: DataTypes.INTEGER,
    montoOperacion: DataTypes.INTEGER
  }, {});
  BolsaPuntos.associate = function(models) {
    // associations can be defined here
  };
  return BolsaPuntos;
};