'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsoPuntos = sequelize.define('UsoPuntos', {
    clienteId: DataTypes.INTEGER,
    puntajeUtilizado: DataTypes.INTEGER,
    fecha: DataTypes.DATEONLY,
    conceptoId: DataTypes.INTEGER
  }, {});
  UsoPuntos.associate = function(models) {
    // associations can be defined here
  };
  return UsoPuntos;
};