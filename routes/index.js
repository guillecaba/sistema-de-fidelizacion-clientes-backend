/* var express = require('express');
var router = express.Router();

/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});*/
/*
module.exports = router;
 */

var router = require('express').Router();

router.use('/api', require('./api'));

module.exports = router;